package com.kraftwerking.spring.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ApplicationContext context = new ClassPathXmlApplicationContext("com/kraftwerking/spring/test/beans/beans.xml");
		Person person1 = (Person) context.getBean("person");
		Person person2 = (Person) context.getBean("person");
		Address address = (Address) context.getBean("address");
		person1.setTaxId(666);
		person1.speak();
		System.out.println(person2);
		System.out.println(address);
		Address address2 = (Address) context.getBean("address2");
		System.out.println(address2);
		
		((ClassPathXmlApplicationContext) context).close();
	}

}
